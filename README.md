# Gitlab Runner for Kubernetes

A deployment of Gitlab Running
https://docs.gitlab.com/runner/install/kubernetes.html

## Setup

to create a config please run ```docker run -ti -v `pwd`/config:/etc/gitlab-runner gitlab/gitlab-runner:alpine-v9.0.0 register```

to use Docker in Docker and the internal registry add `environment = ["DOCKER_HOST=tcp://docker.container-image-builder.svc:2375", "DOCKER_REGISTRY_INTERNAL=registry.registry.svc:5000"]`

## Usage

run `make` for usage

## Access

The Runner executes tasks defined within `.gitlab-ci.yml`. Pipeline executions are managed by the gitlab server.
